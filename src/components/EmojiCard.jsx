export function EmojiCard({title, symbol, keywords}) {
    return(
        <div className="card text-center">
            <div className="card-header">
                {title}
            </div>
            <div className="card-body">
                <h2 className="card-title">{symbol}</h2>
                <p className="card-text">{keywords}</p>
            </div>
        </div>
    );
}