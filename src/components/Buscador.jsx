export function Buscador({valorBuscado, onInputChange}){
    return(
        <div className="row d-flex justify-content-end">
            <div className="col-6">
                <div className="mb-3">
                    <input 
                        type="text" 
                        className="form-control" 
                        placeholder="Ingrese el emoji a buscar"
                        value={valorBuscado}
                        onChange={onInputChange}
                    />
                </div>
            </div>
        </div>
    );
}