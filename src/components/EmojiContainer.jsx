import { EmojiCard } from "./EmojiCard";

export function EmojiContainer({datos}){
    let lecturaDatos = datos.map((emoji)=>{
        return(
            <div className="col-3" key={emoji.title}>
                <EmojiCard 
                    title={emoji.title}
                    symbol={emoji.symbol}
                    keywords={emoji.keywords}
                />
            </div>
        );
    });

    return(
        <div className="row py-4">
            {lecturaDatos}
        </div>
    );
}