import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { TituloEncabezado} from './components/TituloEncabezado';
import { Buscador } from './components/Buscador';
import { EmojiContainer } from './components/EmojiContainer';
import Pagination from "react-js-pagination";

import { useState, useEffect } from 'react';

let LIMIT = 24;
let URL= `http://localhost:3001/emojis?_limit=${LIMIT}`;

function App() {

  let [emojis,listarEmojis] = useState([]);
  let [buscar, buscarEmoji] = useState("");
  let [pagina, setPagina] = useState(1);

//Se consumen los datos de la API de emojis
useEffect(()=>{
  fetch(`${URL}&_page=${pagina}`)
    .then((respuesta)=>respuesta.json() )
    .then((datos)=>{
      listarEmojis(datos);
    })
},[pagina]);

  //Se filtra el listado de emojis que provienen de la API para que coincidan con los que
  //el usuario escribe en el buscador
  let filtrarDatos = emojis.filter((emoji)=>{
    let buscarLowerCase = buscar.toLowerCase();
    let emojiTitleLowerCase = emoji.title.toLowerCase();

    if(emojiTitleLowerCase.includes(buscarLowerCase)){
      return emoji
    };
  });

  //Se capturan los datos ingresados en el buscador y se pasan al useState del mismo
  function emojiBuscado(evento){
    let emojiTypeado = evento.target.value;
    buscarEmoji(emojiTypeado);  
  }
  
  
  function handlePageChange(nuevaPagina) {
    setPagina(nuevaPagina) 
  }
  
  
  return (
    <div className="container">
      <TituloEncabezado/>
      <Buscador value={buscar} onInputChange={emojiBuscado}/>
      {/*Se valida si el buscador esta vacio, si lo esta se muestra todo y si no se muestra lo que coincide con 
        la busqueda*/}
      <EmojiContainer datos={buscar ? filtrarDatos : emojis}/>

    <div className='d-flex justify-content-center'>
      <Pagination
            itemClass="page-item"
            linkClass="page-link"
            activePage={pagina}
            itemsCountPerPage={LIMIT}
            totalItemsCount={1820}
            pageRangeDisplayed={5}
            onChange={handlePageChange}
          />
      </div>
    </div>
  );
}

export default App
